# NAudio Microphone Toggler

Playground for toying about with the .NET [naudio/naudio] library.
A background service throttles the windows default capture device mute state.

[naudio/naudio]: https://github.com/naudio/NAudio

## Requirements

* `Windows OS >= Vista`
* `.NET Runtime >= 6.0.7` or `.NET SDK >= 6.0.302`

The [NAudio Library][naudio/naudio] relies on [Windows Management Instrumentation][WMI]
and is therefore primarily windows compatible only.

[wmi]: https://docs.microsoft.com/en-us/windows/win32/wmisdk/about-wmi

## Running

```bash
dotnet build --project NaudioMicrophoneToggler --output ./release
dotnet ./release/NaudioMicrophoneToggler.dll
```
### Linux
dotnet run --project NaudioMicrophoneToggler
```
Unhandled exception. System.NotSupportedException: This functionality is only supported on Windows Vista or newer.
   at NAudio.CoreAudioApi.MMDeviceEnumerator..ctor()
   ...
```

### Windows

![gif showing the running app as it toggles the windows microphone icon on and off](./images/naudio-device-toggler.gif)