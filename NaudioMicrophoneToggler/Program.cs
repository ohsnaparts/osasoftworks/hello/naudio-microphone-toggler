IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddHostedService<ToggleDeviceMuteStateWorker>();
    })
    .Build();

await host.RunAsync();
