using NAudio.CoreAudioApi;

/// <summary>
/// Detects the currently selected default capture device (microphone)
/// and toggles its mute state in a configured period for demo purposes.
/// This software uses the NAudio library that, unfortunately, highly relies
/// on Windows OS specifics, therefore, unless  
/// </summary>
public class ToggleDeviceMuteStateWorker : BackgroundService
{
    private readonly ILogger _logger;
    private static readonly TimeSpan ToggleInterval = TimeSpan.FromSeconds(1);

    public ToggleDeviceMuteStateWorker(ILogger<ToggleDeviceMuteStateWorker> logger)
    {
        this._logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using var enumerator = new MMDeviceEnumerator();
        this.LogAvailableAudioDevices(enumerator);

        var captureDevice = GetDefaultCommunicationsCaptureAudioEndpoint(enumerator);
        enumerator.RegisterEndpointNotificationCallback(new LoggingNotificationClient(this._logger));
        
        this.LogDeviceSummary(captureDevice);
        while (!stoppingToken.IsCancellationRequested)
        {
            if (Muted(captureDevice))
            {
                this.Unmute(captureDevice);
            }
            else
            {
                this.Mute(captureDevice);
            }
            
            this.LogDeviceSummary(captureDevice);
            await Task.Delay(ToggleInterval, stoppingToken);
        }
    }

    private static MMDevice GetDefaultCommunicationsCaptureAudioEndpoint(MMDeviceEnumerator enumerator) => enumerator
        .GetDefaultAudioEndpoint(DataFlow.Capture, Role.Communications);

    private void SetMuteState(MMDevice device, bool muted)
    {
        this._logger.LogDebug("Updating mute state of device {DeviceId} to {MuteState}", device.ID, muted);
        device.AudioEndpointVolume.Mute = muted;
    }

    private static bool Muted(MMDevice device) => device.AudioEndpointVolume.Mute;
    private void Mute(MMDevice device) => this.SetMuteState(device, true);
    private void Unmute(MMDevice device) => this.SetMuteState(device, false);

    private void LogAvailableAudioDevices(MMDeviceEnumerator deviceEnumerator)
    {
        var wasapiDevices = deviceEnumerator
            .EnumerateAudioEndPoints(DataFlow.All, DeviceState.All)
            .Where(device => device.State != DeviceState.NotPresent);
        
        this._logger.LogInformation("Available devices:");
        foreach (var wasapiDevice in wasapiDevices)
        {
            this._logger.LogInformation(
                "{DataFlow} {FriendlyName} {DeviceFriendlyName} {State}",
                wasapiDevice.DataFlow,
                wasapiDevice.FriendlyName,
                wasapiDevice.DeviceFriendlyName,
                wasapiDevice.State
            );
        }
    }

    private void LogDeviceSummary(MMDevice device) => this._logger.LogInformation(
        "Device {Id} {Name} is {MutedState}",
        device.ID,
        device.FriendlyName,
        Muted(device) ? "muted" : "unmuted"
    );
}