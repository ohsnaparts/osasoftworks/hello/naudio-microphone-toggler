using NAudio.CoreAudioApi;
using NAudio.CoreAudioApi.Interfaces;

public class LoggingNotificationClient : IMMNotificationClient
{
    private readonly ILogger _logger;

    public LoggingNotificationClient(ILogger logger)
    {
        this._logger = logger;
    }

    public void OnDeviceStateChanged(string deviceId, DeviceState newState)
    {
        this._logger.LogInformation("Device {DeviceId} changed", deviceId);
    }

    public void OnDeviceAdded(string pwstrDeviceId)
    {
        this._logger.LogInformation("Device {DeviceId} added", pwstrDeviceId);
    }

    public void OnDeviceRemoved(string deviceId)
    {
        this._logger.LogInformation("Device {DeviceId} removed", deviceId);
    }

    public void OnDefaultDeviceChanged(DataFlow flow, Role role, string defaultDeviceId)
    {
        this._logger.LogInformation("Default device registered: {DeviceId}", defaultDeviceId);
    }

    public void OnPropertyValueChanged(string pwstrDeviceId, PropertyKey key)
    {
        this._logger.LogInformation("Device property {Key} of {DeviceId} changed", key, pwstrDeviceId);
    }
}